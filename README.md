# pixxon-demo-lib

To fetch this package with Buckaroo, do:

```bash
buckaroo add gitlab.com/njlr/pixxon-demo-lib@branch=master
```

## Building

```bash
mkdir build
cd build
cmake ..
make
```
